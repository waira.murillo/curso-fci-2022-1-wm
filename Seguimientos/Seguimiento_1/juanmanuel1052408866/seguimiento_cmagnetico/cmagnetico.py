
import numpy as np

class magnetico:

    # construimos metodo constructor:

    # inicializo atributos.


    def __init__ (self,ang,v,B,alfha0,carga,m,t,px0,py0,pz0): 

        self.ang = ang  # angulo de salida 

        self.v= v   # rapidez de la partícula en el campo magnético.

        self.B = B    # campo magnético ( B)

        self.alfha0 =  alfha0  # posición angular inicial.

        self.carga = carga  # carga de la partícula.

        self.m = m     # masa de la partícula.

        self.t = t    # tiempo

        self.pz0 = pz0 # posición inicial en z.

        self.px0 = px0  # posición inicial en x.

        self.py0 = py0 # posición inicial en y.


        ## ahora se definen métodos que dependen del tiempo:


# mycampo = magnetico("tetha0")

#print(mycampo.tetha0)


      # def posangular(self):

    def velz(self):                  ## define velocidad en z.


        self.vz = self.v*np.cos(self.ang * np.pi/180.0)

        return np.round(self.vz)   # redondea con 3 cifras decimales.


    def posz(self):   # posicion en z.
          
        self.pz = self.pz0 + self.velz() * self.t

        return self.pz



    def posang(self): # determina posicion angular en cualquier instante de t (plano xy)


        self.pang = self.alfha0 * np.pi/180.0 + (self.carga*self.B*self.t)/self.m 


        return (self.pang)


    def Radio(self):  # Determina radio de órbita circular en el plano xy

        self.R =  ( self.m * self.v * np.sin(self.ang * np.pi/180.0) )/(self.m)

        return( self.R)

    

    def posx(self):   # Determina posición en x para cualquier instante t.

        self.px =  self.px0 + self.Radio() * np.cos(self.posang())

        return ( self.px)

    def posy(self):  # Determina posición en y ( depende implícitamente de t)


        self.py = self.py0 + self.Radio() * np.sin(self.posang())


        return (self.py)


    



          
         


