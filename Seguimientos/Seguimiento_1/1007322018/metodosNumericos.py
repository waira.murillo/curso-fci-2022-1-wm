import sympy 
import numpy as np

class metodosNumericos:
    
    def __init__(self,x0,y0,X,f):
        self.x0 = x0
        self.y0 = y0
        self.X = X
        self.f = f
        self.h = 0.1
        self.n = (self.X - self.x0)/self.h

        

    def metodoEuler(self):
        
        x = np.zeros(int(self.n) +1)
        y = np.zeros(int(self.n) +1)
        x[0] = self.x0
        y[0] = self.y0
        

        for i in range(int(self.n) ):
        
           x[i+1] = x[i] + self.h
           y[i+1] = self.h*self.f(x[i],y[i]) + y[i]
        return y[-1] 


    def runge_kutta(self):
        
        x = np.zeros(int(self.n) +1)
        y = np.zeros(int(self.n) +1)
        x[0] = self.x0
        y[0] = self.y0
        k1 = np.zeros(int(self.n) +1)
        k2 = np.zeros(int(self.n) +1)
        k3 = np.zeros(int(self.n) +1)
        k4 = np.zeros(int(self.n) +1)
        
        for i in range(int(self.n)):
        
           x[i+1] = x[i] + self.h

           k1[i] = self.h*self.f(x[i],y[i])
           k2[i] = self.h*self.f(x[i]+0.5*self.h,y[i]+0.5*k1[i])
           k3[i] = self.h*self.f(x[i]+0.5*self.h,y[i]+0.5*k2[i])
           k4[i] = self.h*self.f(x[i]+self.h,y[i]+k3[i])

           y[i+1] = (1/6)*(k1[i]+2*k2[i]+2*k3[i]+k4[i]) + y[i]

        return y[-1]


    def solucion_analitica(self):
        
        x = sympy.Symbol('x')
        y = sympy.Function('y')  
        ics = {y(self.x0): self.y0}
        S=sympy.dsolve(y(x).diff(x) - self.f(x,y(x)),ics=ics)
        J = S.replace(x,self.X)
        return J
    





        
