import numpy as np
import sympy as smp
from scipy.integrate import odeint
import matplotlib.pyplot as plt
from scipy import integrate

class particula:

     def __init__(self,m,q,Ek,theta,B,t):
         self.m = m
         self.q = q
         self.Ek = Ek
         self.theta = theta
         self.B = B
         self.t = t
         self.w = (self.q*self.B)/self.m
         self.v0 = np.sqrt(2*Ek/m) 
         self.v0z = self.v0*np.cos(theta)
         self.v0y = self.v0*np.sin(theta)
         self.v0x = 0

     def trayectoria(self):

         t, w = smp.symbols('t w')


         x, y, z = smp.symbols(r'x, y, z', cls=smp.Function)

         x = x(t)                 #Variable
         x_d = smp.diff(x, t)     #Primera Derivada
         x_dd = smp.diff(x_d, t)  #Segunda Derivada

         y = y(t)                           #Variable
         y_d = smp.diff(y, t)               #Primera Derivada     
         y_dd = smp.diff(y_d, t) #Segunda Derivada

         z = z(t)                            #Variable
         z_d = smp.diff(z, t)                #Primera Derivada
         z_dd = smp.diff(z_d, t)  #Segunda Derivada



         ec_1 = w*y_d - x_dd
         ec_2 = -w*x_d - y_dd
         ec_3 = z_dd

         sols = smp.solve([ec_1,ec_2,ec_3], (x_dd, y_dd, z_dd),simplify=False, rational=False)    #Se despejan las segundas derivadas en cada ecuación de movimiento
                   


         dw1dt_f = smp.lambdify((y_d,w), sols[x_dd])
         dw2dt_f = smp.lambdify((x_d,w), sols[y_dd])
         dw3dt_f = smp.lambdify(w, sols[z_dd])


         def dSdt(S, t):
             x_d, y_d, z_d= S
             return [dw1dt_f(y_d,w), dw2dt_f(x_d,w), dw3dt_f(w)]

         t1 = np.arange(0,self.t,0.001)
         w = self.w
         Y0 = [self.v0x,self.v0y,self.v0z]
         ans = odeint(dSdt,Y0,t1)


         Z_int = integrate.cumtrapz(ans.T[2], t1, dx=0.001,initial=0)
         X_int = integrate.cumtrapz(ans.T[0], t1, dx=0.001,initial=0)
         Y_int = integrate.cumtrapz(ans.T[1], t1, dx=0.001,initial=0)

         return X_int,Y_int,Z_int     
     
     