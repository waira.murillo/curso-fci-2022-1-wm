min=1
max=20
fijo=10
echo Tiene 10 intentos para adivinar el número.
echo  Ingrese un número entero en el rango de 1 a 20:
for numero in {1..10..1};
do
read A
if [[ $A -lt $min ]];
then
echo Error! Ingrese un número en el rango establecido
fi
if [[ $A -gt $max ]];
then
echo Error! Ingrese un número en el rango establecido
fi
if [[ $A -gt $fijo ]];
then
echo Ingrese un numero menor
fi
if [[ $A -lt $fijo ]];
then
echo Ingrese un numero mayor
fi
if [[ $A -eq $fijo ]];
then
echo Ese es el número!!
break
fi
done